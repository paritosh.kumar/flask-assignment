from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

# import login middleware 
from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('todo', __name__)

# route for getting all the todos
@bp.route('/')
def index():
    db = get_db()
    todos = db.execute(
        'SELECT t.id, title, created, user_id, username, status'
        ' FROM todos t JOIN user u ON t.user_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    return render_template('todo/index.html', todos=todos)

# route for creating a todo task
@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        error = None
        if not title:
            error = 'Title is required.'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO todos (title, user_id, status)'
                ' VALUES (?, ?, ?)',
                (title, g.user['id'], 0)
            )
            db.commit()
            return redirect(url_for('todo.index'))

    return render_template('todo/create.html')

def get_todo(id, check_user=True):
    todo = get_db().execute(
        'SELECT t.id, title, created, user_id, username, status'
        ' FROM todos t JOIN user u ON t.user_id = u.id'
        ' WHERE t.id = ?',
        (id,)
    ).fetchone()

    if todo is None:
        abort(404, "Todo id {0} doesn't exist.".format(id))

    if check_user and todo['user_id'] != g.user['id']:
        abort(403)

    return todo

# route for updating a todo task
@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    todo = get_todo(id)

    if request.method == 'POST':
        title = request.form['title']
        error = None

        if not title:
            error = 'Title is required.'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE todos SET title = ?'
                ' WHERE id = ?',
                (title, id)
            )
            db.commit()
            return redirect(url_for('todo.index'))

    return render_template('todo/update.html', todo=todo)

# route for deleting a todo task
@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_todo(id)
    db = get_db()
    db.execute('DELETE FROM todos WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('todo.index'))

# route for changing the done status of a todo task
@bp.route('/<int:id>/toggle', methods=('POST',))
@login_required
def toggle(id):
    get_todo(id)
    db = get_db()
    db.execute('UPDATE todos SET status = ((status | 1) - (status & 1)) WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('todo.index'))