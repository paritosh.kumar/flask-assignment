# Flask Todo List App
Made using flask and jinja template engine. The UI is made using Bootstrap.

## App Features
- The user can register and login.
- Each authenticated user can create, update and delete its todo task.
- Uses Sqlite3 database
- For login middleware, the ```user _id``` gets stored in session and that is used for every authenticated call to the application. 

## How to run

- Create virtual env ```python3 -m venv env```
- Run virtual env 
    - ```source env/bin/activate``` for macOS and Linux
    - ```.\env\Scripts\activate``` for widnows
- Install the requirements.txt file in the current running environment ```pip install -r requirements.txt```
- Before running the app, we setup the FLASK_APP and FLASK_DEVELOPMENT variable. Then we run the app using ```flask run```

For Linux and Mac
```
$ export FLASK_APP=flaskr
$ export FLASK_ENV=development
$ flask run
```
For Windows cmd
```
> set FLASK_APP=flaskr
> set FLASK_ENV=development
> flask run
```

- Initialize the database using ```flask init-db``` command in terminal. This define the tables.

## App structue
- The templates are located in flaskr/templates
- ```flaskr/auth.py``` contains the authentication middlewares.
- ```flaskr/todo.py``` contains the todo routes.
- ```flaskr/db.py``` contains the database functionaltites for initializing, closing and accessing.
- ```flaskr/shema.sql``` contains the schema of the database

## Routes
- ```/``` shows all todos. If the user is loged in it will show edit and mark functionality plus only its tasks. If not then it will show all the todos.
- ```/auth/login``` is the login page url
- ```/auth/register``` registers the user
- ```/create``` creates a new todo (uses login middleware)
- ```/<int:id>/update``` url is for updating the title and shows the delete button



